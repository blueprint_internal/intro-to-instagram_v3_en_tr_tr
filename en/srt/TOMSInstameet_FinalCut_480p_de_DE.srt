﻿1
00:00:07,200 --> 00:00:10,380
Wir befinden uns im TOMS Abbot Kinney Flagship Store

2
00:00:10,380 --> 00:00:13,740
bei unserem InstaMeet-/Venice Street-Ausverkauf.

3
00:00:13,740 --> 00:00:16,940
Bei TOMS dreht sich alles darum, der Gemeinschaft etwas zurückzugeben,

4
00:00:16,940 --> 00:00:19,480
anderen Gutes zu tun, eine globale Botschaft zu verbreiten

5
00:00:19,480 --> 00:00:21,400
und gemeinsam eine bessere Zukunft zu gestalten.

6
00:00:22,520 --> 00:00:24,940
Wir lieben Instagram und InstaMeet war für uns

7
00:00:24,940 --> 00:00:27,600
die logische Konsequenz: Menschen davon zu überzeugen,

8
00:00:27,600 --> 00:00:28,940
etwas Gutes für die Gemeinschaft zu leisten.

9
00:00:28,940 --> 00:00:30,840
Deshalb bestellten wir alle nach Venice.

10
00:00:30,840 --> 00:00:32,700
Es passt perfekt zu TOMS, der Gemeinschaft

11
00:00:32,700 --> 00:00:34,180
etwas zurückzugeben, die für

12
00:00:34,180 --> 00:00:35,870
uns so viel getan hat.

13
00:00:35,870 --> 00:00:38,040
Ich folge Surf-Fotografen in L.A.

14
00:00:38,040 --> 00:00:39,780
und es ist toll, hier dabei zu sein, anstatt

15
00:00:39,780 --> 00:00:42,500
nur Fotos und Bildunterschriften zu sehen.

16
00:00:42,500 --> 00:00:44,300
Wir haben uns hier getroffen und machen coole Fotos,

17
00:00:44,300 --> 00:00:45,780
hier ist einfach eine Gruppe Fremder aus dem Internet,

18
00:00:45,780 --> 00:00:47,740
die eine gute Zeit miteinander verbringen.

19
00:00:49,030 --> 00:00:52,660
Ich komme aus Guatemala und dieses Event ist fantastisch.

20
00:00:52,660 --> 00:00:54,300
Ich glaube, bei der Marke TOMS geht es um...

21
00:00:54,300 --> 00:00:55,600
die Vorstellung, eine einfache Entscheidung

22
00:00:55,600 --> 00:00:57,850
treffen zu können, die das Leben anderer beeinflussen kann...

23
00:00:57,850 --> 00:00:59,140
und zwar ziemlich unmittelbar.

24
00:00:59,140 --> 00:01:00,900
Es gibt also diese Art Kernbotschaften,

25
00:01:00,900 --> 00:01:02,440
die wir anbieten möchten.

26
00:01:02,440 --> 00:01:04,300
Dann gibt es die abenteuerliche Seite, so geht

27
00:01:04,300 --> 00:01:05,760
von unserer Marke eine gewisse Intention aus

28
00:01:05,760 --> 00:01:08,200
sowie ein hoher Grad an Intimität.

29
00:01:08,200 --> 00:01:09,540
Mit Instagram können die Menschen im wahrsten Sinne des Wortes

30
00:01:09,540 --> 00:01:10,500
mit der Marke interagieren.

31
00:01:10,500 --> 00:01:11,900
Sie können dabei im Büro sein

32
00:01:11,900 --> 00:01:13,300
oder zu Hause und wir können ihnen

33
00:01:13,300 --> 00:01:16,020
eine Welt zeigen, die sich außerhalb ihrer Perspektive befindet.

34
00:01:17,540 --> 00:01:19,400
Eine Bewegung beginnt immer mit einer einfachen Idee,

35
00:01:19,400 --> 00:01:20,520
die man jedoch nicht allein umsetzen kann.

36
00:01:20,520 --> 00:01:22,820
Deshalb begeistert uns unsere Gemeinschaft – sie ist es,

37
00:01:22,820 --> 00:01:25,320
die die Bewegung weiter vorantreibt.

