﻿1
00:00:07,200 --> 00:00:10,380
Estamos aqui na loja conceito da TOMS no Abbot Kinney

2
00:00:10,380 --> 00:00:13,740
para nossa organização no InstaMeet, na Venice Street.

3
00:00:13,740 --> 00:00:16,940
A TOMS é sinônimo de comunidade: retribuir,

4
00:00:16,940 --> 00:00:19,480
fazer o bem para as pessoas, divulgar uma mensagem global

5
00:00:19,480 --> 00:00:21,400
e uma forma de construirmos juntos um mundo melhor.

6
00:00:22,520 --> 00:00:24,940
Adoramos o Instagram e o InstaMeet é muito

7
00:00:24,940 --> 00:00:27,600
natural para nós; para fazer com que as pessoas façam

8
00:00:27,600 --> 00:00:28,940
algo de bom para a comunidade.

9
00:00:28,940 --> 00:00:30,840
Portanto, trouxemos todos para Venice,

10
00:00:30,840 --> 00:00:32,700
e esse é o jeito TOMS de fazer algo

11
00:00:32,700 --> 00:00:34,180
para retribuir à comunidade, que tem

12
00:00:34,180 --> 00:00:35,870
feito tanto por nós.

13
00:00:35,870 --> 00:00:38,040
Eu sigo fotógrafos de surf em Los Angeles

14
00:00:38,040 --> 00:00:39,780
e foi muito legal estar lá fora,

15
00:00:39,780 --> 00:00:42,500
não só pelas fotos e legendas.

16
00:00:42,500 --> 00:00:44,300
Nós nos reunimos e tiramos fotos legais

17
00:00:44,300 --> 00:00:45,780
e éramos apenas desconhecidos da Internet

18
00:00:45,780 --> 00:00:47,740
passeando e nos divertindo.

19
00:00:49,030 --> 00:00:52,660
Eu sou da Guatemala e esse evento é maravilhoso.

20
00:00:52,660 --> 00:00:54,300
Acho que a marca TOMS é realmente...

21
00:00:54,300 --> 00:00:55,600
trata da ideia de que você pode tomar

22
00:00:55,600 --> 00:00:57,850
uma decisão simples que pode influenciar a vida das pessoas

23
00:00:57,850 --> 00:00:59,140
de forma imediata.

24
00:00:59,140 --> 00:01:00,900
Então, há esse tipo de mensagens essenciais

25
00:01:00,900 --> 00:01:02,440
que gostamos de veicular.

26
00:01:02,440 --> 00:01:04,300
Portanto, há um lado aventureiro, há

27
00:01:04,300 --> 00:01:05,760
uma intencionalidade na nossa marca.

28
00:01:05,760 --> 00:01:08,200
Há também um alto nível de intimidade.

29
00:01:08,200 --> 00:01:09,540
Com o Instagram, as pessoas podem

30
00:01:09,540 --> 00:01:10,500
interagir com ela.

31
00:01:10,500 --> 00:01:11,900
Elas podem estar no trabalho

32
00:01:11,900 --> 00:01:13,300
ou em casa, e nós podemos mostrar

33
00:01:13,300 --> 00:01:16,020
um mundo fora de sua própria perspectiva.

34
00:01:17,540 --> 00:01:19,400
Um movimento começa apenas com uma ideia simples,

35
00:01:19,400 --> 00:01:20,520
mas você não pode fazer isso sozinho.

36
00:01:20,520 --> 00:01:22,820
Assim, nós amamos nossa comunidade e é ela

37
00:01:22,820 --> 00:01:25,320
que mantém o movimento ativo.

