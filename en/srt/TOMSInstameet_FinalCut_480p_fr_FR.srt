﻿1
00:00:07,200 --> 00:00:10,380
Nous sommes au flagship store TOMS sur Abbot Kinney

2
00:00:10,380 --> 00:00:13,740
pour notre InstaMeet/nettoyage de Venice.

3
00:00:13,740 --> 00:00:16,940
La communauté est primordiale chez TOMS : donner,

4
00:00:16,940 --> 00:00:19,480
aider les autres, faire passer un message à l’échelle mondiale

5
00:00:19,480 --> 00:00:21,400
et construire un meilleur avenir ensemble.

6
00:00:22,520 --> 00:00:24,940
Nous adorons Instagram,

7
00:00:24,940 --> 00:00:27,600
alors nous proposons un InstaMeet pour encourager les gens

8
00:00:27,600 --> 00:00:28,940
à agir pour le bien de la communauté.

9
00:00:28,940 --> 00:00:30,840
Nous voilà donc réunis à Venice.

10
00:00:30,840 --> 00:00:32,700
Cette façon de faire est typique de TOMS :

11
00:00:32,700 --> 00:00:34,180
la communauté nous a beaucoup donné

12
00:00:34,180 --> 00:00:35,870
et nous lui rendons la pareille.

13
00:00:35,870 --> 00:00:38,040
Je suis des photographes de surf à Los Angeles

14
00:00:38,040 --> 00:00:39,780
et c’était super d’y être,

15
00:00:39,780 --> 00:00:42,500
ce n’étaient pas que des photos et des légendes.

16
00:00:42,500 --> 00:00:44,300
Nous nous réunissons pour prendre de belles photos,

17
00:00:44,300 --> 00:00:45,780
entre inconnus du net,

18
00:00:45,780 --> 00:00:47,740
et nous passons un bon moment.

19
00:00:49,030 --> 00:00:52,660
Je viens du Guatemala et cet évènement est génial !

20
00:00:52,660 --> 00:00:54,300
Le concept de la marque TOMS,

21
00:00:54,300 --> 00:00:55,600
c’est qu’une simple décision

22
00:00:55,600 --> 00:00:57,850
peut avoir un impact sur les autres

23
00:00:57,850 --> 00:00:59,140
de façon plutôt immédiate.

24
00:00:59,140 --> 00:01:00,900
Alors nous aimons

25
00:01:00,900 --> 00:01:02,440
faire passer ces messages profonds.

26
00:01:02,440 --> 00:01:04,300
Il y a un côté aventure,

27
00:01:04,300 --> 00:01:05,760
une intention derrière notre marque.

28
00:01:05,760 --> 00:01:08,200
Mais aussi une intimité bien présente

29
00:01:08,200 --> 00:01:09,750
avec laquelle les gens peuvent interagir

30
00:01:09,750 --> 00:01:10,500
grâce à Instagram.

31
00:01:10,500 --> 00:01:11,900
Ils peuvent être au bureau

32
00:01:11,900 --> 00:01:13,300
ou chez eux, et nous pouvons leur montrer

33
00:01:13,300 --> 00:01:16,020
un monde différent du leur.

34
00:01:17,540 --> 00:01:19,400
Un mouvement commence par une idée,

35
00:01:19,400 --> 00:01:20,520
mais vous ne pouvez pas agir seul.

36
00:01:20,520 --> 00:01:22,820
Nous adorons notre communauté,

37
00:01:22,820 --> 00:01:25,320
elle nous aide à garder le cap.

