﻿1
00:00:07,388 --> 00:00:10,154
Şu anda Instameet/Venice cadde temizliği etkinliğimiz için

2
00:00:10,154 --> 00:00:13,757
TOMS'un en büyük mağazası olan Abbot Kinney mağazasındayız.

3
00:00:13,757 --> 00:00:18,306
TOMS için en önemli olan şey topluluk. Marka, topluluğa bir şeyler sunmak,

4
00:00:18,306 --> 00:00:19,454
diğerleri için iyi şeyler yapmak ve daha iyi bir geleceği

5
00:00:19,454 --> 00:00:21,985
birlikte yaratma mesajını tüm dünyaya yaymak için çalışıyor.

6
00:00:21,985 --> 00:00:25,549
Instagram'ı çok seviyoruz ve Instameet buluşmasında insanları toplulukları için

7
00:00:25,549 --> 00:00:28,826
iyi bir şeyler yapmaya teşvik etmemiz çok doğaldı.

8
00:00:28,826 --> 00:00:32,048
Bunun için herkesi Venice'te topladık.

9
00:00:32,048 --> 00:00:35,778
Bizim için çok şey yapmış bir topluluğa bir şeyler vermek tam da TOMS'a göre bir şey.

10
00:00:35,778 --> 00:00:37,899
Los Angeles'lı bazı fotoğrafçıları takip ediyorum.

11
00:00:37,899 --> 00:00:41,439
Her şeyin sadece fotoğraflardan ve açıklamalardan ibaret olmadığını görmek harikaydı.

12
00:00:42,523 --> 00:00:44,243
Bir araya geliyor, fotoğraflar çekiyoruz.

13
00:00:44,243 --> 00:00:47,019
İnternette tanışan bir sürü yabancının takıldığı ve güzel zaman geçirdiği bir etkinlik bu.

14
00:00:49,169 --> 00:00:50,146
Ben Guatemala'dan geliyorum.

15
00:00:50,146 --> 00:00:51,697
Bu etkinlik müthiş.

16
00:00:52,901 --> 00:00:54,235
Bence TOMS markasının basit bir fikri var.

17
00:00:54,235 --> 00:00:56,215
Verdiğin basit bir karar, birilerinin

18
00:00:56,215 --> 00:00:59,052
hayatını anında etkileyebilir.

19
00:00:59,052 --> 00:01:02,316
İletmek istediğimiz bazı temel mesajlarımız var.

20
00:01:02,316 --> 00:01:03,835
İşin macera kısmı var.

21
00:01:03,835 --> 00:01:05,838
Markamızın bir amacı bulunuyor.

22
00:01:05,838 --> 00:01:07,688
Aynı zamanda çok yüksek bir samimiyet duygusu da var.

23
00:01:07,688 --> 00:01:10,388
Instagram'la insanlar markayla gerçek anlamda etkileşimde bulunabiliyorlar.

24
00:01:10,388 --> 00:01:11,792
Ofislerinde olabilirler.

25
00:01:11,792 --> 00:01:12,519
Evlerinde olabilirler.

26
00:01:12,519 --> 00:01:15,423
Biz onlara kendi dünyaları dışında bir dünyayı gösterebiliriz.

27
00:01:17,502 --> 00:01:20,565
Bir hareket basit bir fikirle başlayabilir, ancak bunu yalnız başaramazsınız.

28
00:01:20,565 --> 00:01:22,011
Topluluğumuzu seviyoruz ve

29
00:01:22,011 --> 00:01:24,092
bu hareketin arkasındaki itici gücü oluşturuyor.

