﻿1
00:00:07,200 --> 00:00:10,380
Kami sedang berada di toko utama TOMS Abbot Kinney

2
00:00:10,380 --> 00:00:13,740
untuk menghadiri Instameet/membersihkan Jalanan Venesia.

3
00:00:13,740 --> 00:00:16,940
TOMS adalah tentang komunitas: membalas,

4
00:00:16,940 --> 00:00:19,480
melakukan hal baik bagi orang lain, menyebarkan pesan global

5
00:00:19,480 --> 00:00:20,368
dan bagaimana kita bersama-sama

6
00:00:20,368 --> 00:00:21,424
membangun masa depan yang lebih baik.

7
00:00:22,520 --> 00:00:24,940
Kami menyukai Instagram dan Instameet yang

8
00:00:24,940 --> 00:00:26,373
merupakan sesuatu yang alami; untuk

9
00:00:26,373 --> 00:00:27,600
mengajak orang melakukan sesuatu

10
00:00:27,600 --> 00:00:28,940
yang baik bagi komunitas.

11
00:00:28,940 --> 00:00:30,840
Jadi kami membawa setiap orang ke Venesia

12
00:00:30,840 --> 00:00:32,700
dan ini adalah cara khas TOM untuk melakukan sesuatu

13
00:00:32,700 --> 00:00:34,180
guna membalas kepada komunitas yang

14
00:00:34,180 --> 00:00:35,870
sudah melakukan begitu banyak hal bagi kami.

15
00:00:35,870 --> 00:00:38,040
Saya mengikuti fotografer surfing di LA

16
00:00:38,040 --> 00:00:39,780
dan sangat menyenangkan berada di sana,

17
00:00:39,780 --> 00:00:42,500
bukan sekadar foto dan memberi keterangan.

18
00:00:42,500 --> 00:00:44,300
Kita berkumpul bersama dan mengambil foto bagus

19
00:00:44,300 --> 00:00:45,780
dan semuanya tentang orang-orang asing di internet

20
00:00:45,780 --> 00:00:47,740
yang berjalan-jalan dan bersenang-senang.

21
00:00:49,030 --> 00:00:52,660
Saya dari Guatemala dan acara ini sangat luar biasa.

22
00:00:52,660 --> 00:00:54,300
Menurut saya merek TOMS benar-benar...

23
00:00:54,300 --> 00:00:55,600
ini adalah tentang ide yang dapat Anda lakukan

24
00:00:55,600 --> 00:00:57,850
keputusan sederhana yang berdampak pada kehidupan seseorang

25
00:00:57,850 --> 00:00:59,140
dalam kapasitas saat itu juga.

26
00:00:59,140 --> 00:01:00,900
Jadi ada semacam pesan utama

27
00:01:00,900 --> 00:01:02,440
yang ingin kami layani.

28
00:01:02,440 --> 00:01:04,300
Jadi terdapat sisi petualangan, terdapat

29
00:01:04,300 --> 00:01:05,760
sebuah kesengajaan terhadap merek kami.

30
00:01:05,760 --> 00:01:08,200
Juga terdapat tingkat keakraban yang tinggi.

31
00:01:08,200 --> 00:01:09,540
Dengan Instagram, orang

32
00:01:09,540 --> 00:01:10,500
benar-benar dapat berinteraksi.

33
00:01:10,500 --> 00:01:11,900
Mereka dapat berada di kantornya,

34
00:01:11,900 --> 00:01:12,574
mereka dapat berada di rumah dan

35
00:01:12,574 --> 00:01:13,300
kita dapat menunjukkan kepada mereka

36
00:01:13,300 --> 00:01:16,020
dunia yang berada di luar pandangan mereka.

37
00:01:17,540 --> 00:01:19,400
Sebuah pergerakan dimulai dengan satu ide sederhana

38
00:01:19,400 --> 00:01:20,520
namun Anda tidak dapat melakukan hal ini seorang diri.

39
00:01:20,520 --> 00:01:22,820
Jadi kami mencintai komunitas kami dan mereka benar-benar

40
00:01:22,820 --> 00:01:25,320
membuat semuanya bergerak aktif.

