﻿1
00:00:07,200 --> 00:00:10,280
Estamos en la tienda emblemática TOMS Abbot Kinney

2
00:00:10,380 --> 00:00:13,660
en Venice, para nuestro InstaMeet de limpieza urbana.

3
00:00:13,740 --> 00:00:16,880
La comunidad es la base de TOMS: corresponder,

4
00:00:16,940 --> 00:00:19,420
hacer el bien a los demás, difundir un mensaje global

5
00:00:19,480 --> 00:00:21,400
y construir un mejor mañana entre todos.

6
00:00:22,520 --> 00:00:24,900
Nos encanta Instagram y los InstaMeets se adaptan

7
00:00:24,940 --> 00:00:27,560
a nuestra naturaleza, ya que reúnen a las personas

8
00:00:27,600 --> 00:00:28,880
para hacer algo bueno por la comunidad.

9
00:00:28,940 --> 00:00:30,820
Así que convocamos a todo el mundo en Venice

10
00:00:30,840 --> 00:00:32,640
en un acto muy acorde al estilo de TOMS:

11
00:00:32,700 --> 00:00:34,140
corresponder a la comunidad

12
00:00:34,180 --> 00:00:35,540
que hizo tanto por nosotros.

13
00:00:35,870 --> 00:00:37,980
Sigo a algunos fotógrafos de surf de Los Ángeles

14
00:00:38,040 --> 00:00:39,700
y me gustó mucho participar

15
00:00:39,780 --> 00:00:42,140
y hacer algo más que solo ver fotos y descripciones.

16
00:00:42,500 --> 00:00:44,240
Nos reunimos y tomamos fotos estupendas.

17
00:00:44,300 --> 00:00:45,720
Solo éramos unos cuantos extraños de internet

18
00:00:45,780 --> 00:00:47,740
que nos reunimos para pasar un momento agradable.

19
00:00:49,030 --> 00:00:51,940
Soy de Guatemala y este evento me pareció increíble.

20
00:00:52,660 --> 00:00:54,240
Creo que la marca TOMS

21
00:00:54,300 --> 00:00:55,540
se relaciona con la idea de que puedes tomar

22
00:00:55,600 --> 00:00:57,850
una decisión sencilla que genere un impacto en la vida de alguien

23
00:00:57,850 --> 00:00:59,080
de forma inmediata.

24
00:00:59,140 --> 00:01:00,860
Esta es la clase de mensajes fundamentales

25
00:01:00,900 --> 00:01:02,380
que nos gusta difundir.

26
00:01:02,440 --> 00:01:04,240
Por un lado está la aventura,

27
00:01:04,300 --> 00:01:05,700
la intencionalidad de nuestra marca.

28
00:01:05,760 --> 00:01:07,960
Pero también hay un alto nivel de intimidad. 

29
00:01:08,200 --> 00:01:09,480
Con Instagram, las personas pueden

30
00:01:09,540 --> 00:01:10,460
literalmente interactuar con la marca.

31
00:01:10,500 --> 00:01:11,820
Tal vez estén en su oficina

32
00:01:11,900 --> 00:01:13,240
o en su casa, y nosotros les mostramos

33
00:01:13,300 --> 00:01:16,020
un mundo diferente.

34
00:01:17,540 --> 00:01:19,320
Un movimiento empieza con una idea simple,

35
00:01:19,400 --> 00:01:20,460
pero no se puede llevar a cabo de forma individual.

36
00:01:20,520 --> 00:01:22,760
Apreciamos a nuestra comunidad.

37
00:01:22,820 --> 00:01:25,320
Ellos son los que mantienen vivo el movimiento.