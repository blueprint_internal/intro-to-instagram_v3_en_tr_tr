function TestRun3(target,resources)
{
	this.target = target;
	this.spritesAr = new Array();
	this.textAr = new Array();
	this.resources = resources;
	this.setUpPreloader();
	this.yLocationAdjustment = new Array();
	this.yLocationAdjustment = [0,-15,-20];
}

TestRun3.prototype = {
	
	setUpPreloader: function()
	{

		this.keepAspectRatio = true;
		this.loadedGraphics = 0;
		canvas = document.getElementById("testCanvas3");
		this.stage = new createjs.Stage(canvas);
		this.stage.enableMouseOver(10);
		borderPadding = 10;

		var barHeight = 20;
		loaderColor = createjs.Graphics.getRGB(61, 92, 155);
		loaderBar = new createjs.Container();

		bar = new createjs.Shape();
		bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();

		imageContainer = new createjs.Container();
		imageContainer.x = this.resources.width;
		imageContainer.y = this.resources.height;

		loaderWidth = 300;
		this.stage.addChild(imageContainer);

		var bgBar = new createjs.Shape();
		var padding = 3
		bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);

		loaderBar.x = canvas.width - loaderWidth >> 1;
		loaderBar.y = canvas.height - barHeight >> 1;
		loaderBar.addChild(bar, bgBar);

		this.stage.addChild(loaderBar);
		
		manifest = 
		[
			{src: this.resources.background, id: "background"},
			{src: this.resources.icon_1, id: "icon_1"},
			{src: this.resources.icon_2, id: "icon_2"},
			{src: this.resources.icon_3, id: "icon_3"}
			//{src: "ball.png", id: "ball"}
			
		];
		this.map = new Array();
		this.preload = new createjs.LoadQueue(true);
       
		// Use this instead to use tag loading
		//preload = new createjs.PreloadJS(false);

		this.preload.on("progress", this.handleProgress,this);
		this.preload.on("complete", this.handleComplete,this);
		this.preload.on("fileload", this.handleFileLoad,this);
		//"course/en/animations/02_instagram_community/"
		this.preload.loadManifest(manifest,true);

		createjs.Ticker.setFPS(30);
	},
	
	handleProgress: function(event) {
		bar.scaleX = event.loaded * loaderWidth;
	},

	handleComplete: function(event) {
		this.background = this.map[0];
		this.background.alpha = 0;
		this.stage.addChild(this.background);
		loaderBar.visible = false;
		this.setupAnimation();
		this.stage.update();
       	var fn = this.handleTick.bind(this);
       	var self = this;
       	createjs.Ticker.addEventListener("tick",fn);
	    //function handleTick(event) {
	    	//console.log("tick");

	    	//}
	},

    handleTick: function(event){
    	if(this.stage){this.stage.update()};
    },

	handleFileLoad: function(event) {
		this.map.push(new createjs.Bitmap(event.result))
		this.stage.update();
	},

    setupAnimation: function()
    {
    	textAr = new Array();
    	for(var i = 0; i<3; i++)
		{
			var temp = this.map[i+1];
			temp.alpha = .5;
			temp.x=200;
			temp.y=90+ (140*i);
			temp.origins = {x:temp.x,y:temp.y};
    	    this.spritesAr.push(temp);
    	    this.stage.addChild(temp);
    	    var textContainer = new createjs.Container();
    	    var title = new createjs.Text(this.resources["title_"+(i+1)], "bold 18px freight-sans-pro","#000000");
 			textContainer.x=350;
			textContainer.y=(120+ (150*i))+this.yLocationAdjustment[i];
			textContainer.origins = {x:textContainer.x,y:textContainer.y};
 			title.textBaseline = "alphabetic";
 			//
 			var body = new createjs.Text(this.resources["body_"+(i+1)], "19px freight-sans-pro","#000000");
 			body.lineWidth = 300
 			body.y = 8;
 			title.textBaseline = "alphabetic";


 			textContainer.addChild(title);
 			textContainer.addChild(body);
 			textAr.push(textContainer);
 			this.stage.addChild(textContainer);
		}
    	this.stage.update();
    	
    	this.fadeInBackground();
    },

    resetAnimation: function()
    {
    	var fn = this.handleTick.bind(this);
       	createjs.Ticker.addEventListener("tick",fn);
       	this.fadeInBackground();
    },
    fadeInBackground: function()
    {
    	for(var i = 0; i<3; i++)
		{
			var subj = textAr[i];
			subj.alpha = 0;
			subj.x = subj.origins.x + 200;
			//
			var subj2 = this.spritesAr[i];
			subj2.alpha = 0;
			subj2.x = subj.origins.x - 200;
			//
			createjs.Tween.removeTweens(textAr[i]);
			createjs.Tween.removeTweens(this.spritesAr[i]);
			textAr[i].alpha = 0;
			this.spritesAr[i].alpha = 0;

		}
    	this.background.alpha = 0;
    	createjs.Tween.get(this.background)
	         .to({alpha:1},800,createjs.Ease.backInOut)
	         .call(this.playAnimation,null,this);
    },

    playAnimation: function()
	{
		for(var i = 0; i<3 ; i++)
		{
			var subj = textAr[i];
			subj.x = subj.origins.x + 200;
			subj.y = subj.origins.y;
			createjs.Tween.get(subj)
	         .wait(900*i)
	         .to({alpha:1,x:subj.origins.x,y:subj.origins.y}, 1000,createjs.Ease.backOut)
	         //
	         var subj = this.spritesAr[i];
			subj.alpha = 0;
			subj.x = subj.origins.x - 200;
			subj.y = subj.origins.y;
			createjs.Tween.get(subj)
	         .wait(700*i)
	         .to({alpha:.75,x:subj.origins.x,y:subj.origins.y}, 1000,createjs.Ease.backOut)

	        
		}
		/*
		var self = this;
		createjs.Tween.get(this.logo)
	         .wait(500)
	         .to({alpha:1,x:436,y:260}, 1000,createjs.Ease.elasticOut)
	         .call(function(){self.handleChange()})
	         createjs.Ticker.addEventListener("tick", handleTick);
	         function handleTick(event)
	          {
	          	console.log("tick");
	          	stage.update();

	          }
	    */
	},

	animationComplete: function(event)
	{
		
		//createjs.Ticker.on("tick", myFunction);
		//function myFunction(event) {
		   // event.remove();
		//}
		//debugger
		//createjs.Ticker.removeEventListener("tick", handleTick);
		var fn = this.handleTick.bind(this);
       	createjs.Ticker.removeEventListener("tick",self.handleTick);
	},

    
	

	resize: function(w,h)
	{
	
	var ow = this.resources.width; // your stage width
	var oh = this.resources.height; // your stage height

	if (this.keepAspectRatio == true)
	{
	    // keep aspect ratio
	    var scale = Math.min(w / ow, h / oh);
	    this.stage.scaleX = scale;
	    this.stage.scaleY = scale;

	   // adjust canvas size
	   this.stage.canvas.width = ow * scale;
	   this.stage.canvas.height = oh * scale;
	}
	
	this.stage.update()
	//this.replayAnimation();
	}

}
/*
function addCommas(nStr)
{
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
*/
